;;;; -*- Mode: Lisp -*-

;;;; with-cl-pkg.lisp
;;;; The "with" macro, as it whoculd have been done in CL a long time
;;;; ago.

(defpackage "IT.UNIMIB.DISCO.MA.CL.EXT.DACF.WITH-CONTEXTS" (:use "CL")
  (:nicknames "CL-CONTEXTS" "CTXTS")
  
  (:documentation "The CL Contexts Package.

The package containing and exporting the names relating to the
implementation of the WITH macro and 'contexts' in Common Lisp.
")

  ;; The main class.

  (:export "CONTEXT" "IS-CONTEXT" "CONTEXT-P")


  ;; The main protcol.

  (:export "ENTER" "HANDLE" "EXIT")


  ;; The WITH macro.

  (:export "WITH")


  ;; Other contexts.

  (:export "MANAGED-RESOURCE-CONTEXT"
   "IS-MANAGED-RESOURCE-CONTEXT"
   "MANAGED-RESOURCE-CONTEXT-P"
   "MANAGED-RESOURCE"
   "ACQUIRE"
   "RELEASE"
   )


  (:export "NULL-CONTEXT"
   "IS-NULL-CONTEXT"
   "NULL-CONTEXT-P"
   )

  (:export "DELEGATE-CONTEXT"
   "IS-DELEGATE-CONTEXT"
   "DELEGATE-CONTEXT-P"

   "DELEGATE"
   )

  (:export "CONDITION-IGNORING-CONTEXT"
   "IS-CONDITION-IGNORING-CONTEXT"
   "CONDITION-IGNORING-CONTEXT-P"
   
   "IGNORED"
   "SUPPRESSED"
   )


  (:export "REDIRECT-CONTEXT"
   "IS-REDIRECT-CONTEXT"
   "REDIRECT-CONTEXT-P"

   "REDIRECT-OUTPUT-CONTEXT"
   "REDIRECT-STANDARD-OUTPUT-CONTEXT"
   "REDIRECT-ERROR-OUTPUT-CONTEXT"

   "REDIRECT-INPUT-CONTEXT"

   "REDIRECTION"

   "STANDARD-OUTPUT-REDIRECTION"
   "ERROR-OUTPUT-REDIRECTION"
   "STANDARD-INPUT-REDIRECTION"
   )

  (:export "EXIT-STACK-CONTEXT"
   "IS-EXIT-STACK-CONTEXT"
   "EXIT-STACK-CONTEXT-P"

   "EXIT-STACK"

   "ENTER-CONTEXT"
   "PUSH-CONTEXT"
   "CALLBACK"
   "POP-ALL"
   "UNWIND"
   )

  )

;;;; end of file -- with-contexts-pkg.lisp
