;;;; -*- Mode: Lisp -*-

;;;; suppress-context.lisp
;;;; Another simple example from Python's contextlib.
;;;;
;;;; See file COPYING for copyright and licensing information.


(in-package "CTXTS")

(defclass condition-ignoring-context (context)
  ((conditions :initarg :conditions :initform nil :reader ignored-conditions)
   (check-subtypep :initarg :subtypep :reader check-subtypep)
   )
  (:default-initargs :subtypep nil)
  (:documentation "The Condition-ignoring Context Class.

Instances of this class are used to ignore/suppress errors within a
WITH context macro.
")
  )


(defgeneric is-condition-ignoring-context (x)
  (:documentation "Returns T if X is a CONDITION-IGNORING-CONTEXT, NIL otherwise.")
  (:method ((x condition-ignoring-context)) t)
  (:method ((x t)) nil))


(defun condition-ignoring-context-p (x)
  "Returns T if X is a CONDITION-IGNORING-CONTEXT, NIL otherwise."
  (is-condition-ignoring-context x))


;;; Constructors.

(defun ignored (conditions &key (subtypep nil) &allow-other-keys)
  "Returns a CONDITION-IGNORING-CONTEXT.

The argument CONDITIONS is a list of CONDITION designators (or a
single CONDITION designator) to be ingnored by the HANDLE method
within a WITH context macro.

If SUBTYPEP is T any condition/error that is a subtype/subclass of any
member in CONDITIOS will be ignored/suppressed by the HANDLE method,
otherwse only instances of the exact CONDITIONS member will be
ignored.


See Also:

ENTER, HANDLE, EXIT, SUPPRESSED
"

  (make-instance 'condition-ignoring-context
                 :conditions (etypecase conditions
                               (atom (list conditions))
                               (list conditions))
                 :subtypep subtypep)
  )


(defun suppressed (conditions &key (subtypep nil) &allow-other-keys)
  "Returns a CONDITION-IGNORING-CONTEXT.

This constructor is a synonim for IGNORED.


See Also:

IGNORED, ENTER, HANDLE, EXIT
"
  (ignored conditions :subtypep subtypep))


;;;; ENTER/HANDLE/EXIT protocol.

(defmethod enter ((ign condition-ignoring-context) &key)
  ign)


(defmethod handle ((ign condition-ignoring-context) (e error) &key)
  (let ((cdns (ignored-conditions ign))
        (chksubt (check-subtypep ign))
        (etype (type-of e))
        )
    ;; We will insure that CDNS only contains type specifiers; i.e., symbols.
    ;; Also, a RESTART in WITH may be interestingly setup.
    ;; Let's think about this later.
    (if (some (lambda (c)
                    (if chksubt
                        (typep e c)
                        (eq etype c)))
                  cdns)
        (values nil e) ; Behave as IGNORE-ERRORS.
        (error e))))


(defmethod exit ((ign condition-ignoring-context) &key)
  ign)


;;;; end of file -- suppress-context.lisp
