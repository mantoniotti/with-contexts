;;;; -*- Mode: Lisp -*-

;;;; null-context.lisp
;;;; A very simple example from Python's contextlib.
;;;;
;;;; See file COPYING for copyright and licensing information.


(in-package "CTXTS")

(defclass null-context (context)
  ((enter-result :initarg :enter-result
                 :reader enter-result))
                 
  (:default-initargs :enter-result nil)

  (:documentation "The Null Context Class.

A context that just wraps a value that it is then returned by ENTER.


See Also:

ENTER, HANDLE, EXIT, WITH, NULL-CONTEXT (Function)
")
  )


(defgeneric is-null-context (x)
  (:method ((x null-context)) t)
  (:method ((x t)) nil))


(defun null-context-p (x) (is-null-context x))


;;; Constructors.

(defun null-context (&optional enter-result)
  "Creates a NULL-CONTEXT stashing ENTER-RESULT for ENTER.

A subsequent call to ENTER on the instance will extract ENTER-RESULT and return it.


See Also:

ENTER, HANDLE, EXIT, WITH, NULL-CONTEXT (Class)
"
  (make-instance 'null-context :enter-result enter-result)
  )


;;;; ENTER/HANDLE/EXIT protocol.

(defmethod enter ((nc null-context) &key)
  (enter-result nc))


(defmethod handle ((nc null-context) (e error) &key)
  (call-next-method))


(defmethod exit ((nc null-context) &key)
  (call-next-method))


;;;; end of file -- null-context.lisp
