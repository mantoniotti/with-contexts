;;;; -*- Mode: Lisp -*-

;;;; resource-examples.lisp
;;;; This is the Python example shown with a "decorator".
;;;; It is not quite clear what exactly the @contextmanager decorator
;;;; does in practice.  Most likely it wraps a function with a
;;;; "context" object with __enter__ and __exit__ methods that both
;;;; call the generator (i.e., the said function containig the yield
;;;; statement)
;;;;
;;;; How do we do these things in Common Lisp?
;;;;
;;;; Solution one: we just build "state" objects.
;;;;
;;;; Other solutions are not feasible in Common Lisp given the lack of
;;;; proper (delimited) continuations.
;;;;
;;;;
;;;; Notes:
;;;;
;;;; This code is just and example that should be expanded upon to
;;;; become of any use. As is, it is just a placeholder showing how to
;;;; render, more or less, the Python examples in Common Lisp.
;;;;
;;;; See file COPYING for copyright and licensing information.


(in-package "CTXTS")


;;; managed-resource-context

(defclass managed-resource-context (context)
  ((resource :initarg :resource :initform nil))
  (:documentation "The Managed Resource Context Class.

A placeholder class showing how to render, more or less, the Python
examples in Common Lisp.")
  )


(defgeneric is-managed-resource-context (x)
  (:method ((x managed-resource-context)) t)
  (:method ((x t)) nil)
  (:documentation
   "Returns T if the argument X is a MANAGED-RESOURCE-CONTEXT."))


(defun managed-resource-context-p (x)
  "Returns T if the argument X is a MANAGED-RESOURCE-CONTEXT.

Notes:

This function is a synonim of IS-MANAGED-RESOURCE-CONTEXT."
  (is-managed-resource-context x))


;;; Constructor.

(defun managed-resource (id &key &allow-other-keys)
  "Constructs a MANAGED-RESOURCE.

The only declared parameter, ID, should be a unique identified for the
resource."
  (make-instance 'managed-resource-context :resource id)
  )


;;;; ACQUIRE/RELEASE protocol.

(defgeneric acquire (mr &key)
  (:method ((mr managed-resource-context) &key) mr)
  (:documentation
   "Acquires a MANAGED-RESOURCE.

This generic function is the 'entry' point of the ACQUIRE/RELEASE
protocol."))


(defgeneric release (mr &key)
  (:method ((mr managed-resource-context) &key) mr)
  (:documentation
   "Releases a MANAGED-RESOURCE.

This generic function is the 'exit' point of the ACQUIRE/RELEASE
protocol."))


;;;; ENTER/HANDLE/EXIT protocol.

(defmethod enter ((mr managed-resource-context) &key)
  (acquire mr))


(defmethod handle ((mr managed-resource-context) (e error) &key)
  (call-next-method) ; (error e)
  )

(defmethod exit ((mr managed-resource-context) &key)
  (release mr))


;;;; end of file -- managed-resource-context.lisp
