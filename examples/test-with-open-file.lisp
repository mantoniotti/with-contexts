;;;; -*- Mode: Lisp -*-

;;;; test-with-open-file.lisp --
;;;;
;;;; See file COPYING for copyright and licensing information.
;;;;
;;;; This file contains the WITH-OPEN-FILE test.

(in-package "CTXTS")

(defun test-wof-11 (f &aux fff)
  (with fff = (open f) do
	(format t "WITHCTXI: within WITH the stream is open: ~S~%"
		(open-stream-p fff))
	)

  (format t "WITHCTXI: after WITH is the stream open? ~S~%"
	  (open-stream-p fff)))


;;;; end of file -- test-with-open-file.lisp
