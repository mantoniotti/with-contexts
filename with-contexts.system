;;;; -*- Mode: Lisp -*-

;;;; with-contexts.system
;;;;
;;;; See file COPYING for copyright and licensing information.


(mk:defsystem :with-contexts
  :documentation "The WITH-CONTEXT System.

A system providing a WITH macro and 'context'ualized objects handled
by a ENTER/HANDLE/EXIT protocol in the spirit of Python's WITH macro.

Only better, or, at a minimum different, of course.
"

  :licence "BSD"

  :author "Marco Antoniotti <mantoniotti@common-lisp.net>"

  :components ((:file "with-contexts-pkg")
               (:file "contexts" :depends-on ("with-contexts-pkg"))
               (:file "with-cl" :depends-on ("with-contexts-pkg"
					     "contexts"))
	       (:module "library"
			:depends-on ("contexts" "with-cl")
			:components ((:file "with-open-file")
				     (:file "exit-stack-context")
				     (:file "suppress-context")
                                     (:file "null-context")
                                     (:file "delegate-context")
                                     (:file "redirect-context")
                                     (:file "managed-resource-context")
				     )
			)
               ))

;;;; end of file -- with-contexts.system
